package com.mqttinsight.ui.component;

/**
 * @author ptma
 */
public interface Textable {

    String getText();

}
