package com.mqttinsight.scripting;

public interface ResultListener {

    void onResult(ScriptResult result);
}
