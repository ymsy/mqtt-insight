<img src="assets/logo.png" alt="logo" width="32"/> MqttInsight - MQTT Client GUI
--
![JDK](https://img.shields.io/badge/JDK-17-blue.svg)
![Apache 2.0](https://img.shields.io/badge/Apache-2.0-blue.svg)
![Release](https://img.shields.io/badge/Release-1.0.0-blue.svg)

MqttInsight is a swing based MQTT gui client.

## Features:

* Support MQTT3 and MQTT5;
* Provide two message views: table and dialogue;
* Support Node.js JavaScript (Javet);
* Supports PlainText, JSON, XML, HEX, and Base64 regular message encoding and decoding, as well as SPI extended codecs;

## Screenshots

![Screenshot1](screenshots/table_view.png)
![Screenshot1](screenshots/dialogue_view.png)

## Documents

* [Scripting](Scripting.md)
* [Changelog](Changelog.md)
