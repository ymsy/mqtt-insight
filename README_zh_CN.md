<img src="assets/logo.png" alt="logo" width="32"/> MqttInsight
--
![JDK](https://img.shields.io/badge/JDK-17-blue.svg)
![Apache 2.0](https://img.shields.io/badge/Apache-2.0-blue.svg)
![Release](https://img.shields.io/badge/Release-1.0.0-blue.svg)

MqttInsight 是一个使用 java swing 开发的 MQTT 图形客户端.

## 主要功能

* 支持 MQTT3、MQTT5；
* 支持表格和对话两种消息视图；
* 支持 Node.js 脚本 (Javet)；
* 支持 PlainText、JSON、XML、HEX 和 Base64 常规消息编解码, 支持 SPI 扩展编解码器；

## 界面截图

![Screenshot1](screenshots/table_view.png)
![Screenshot1](screenshots/dialogue_view.png)

## 文档

* [脚本说明](Scripting.md)
* [版本记录](Changelog.md)
